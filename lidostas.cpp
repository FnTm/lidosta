/*
 * File:   main.cpp
 * Author: Janis
 *
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#define SIZE 20001

using namespace std;

class Time
{
public:

    unsigned long int stundas, minutes;

    Time()
    {
    };

    Time(unsigned long int x, long unsigned int y)
    {
        this->stundas = x;
        this->minutes = y;

    }
string makeTime()
{
     stringstream ss;
    if(this->stundas<10){
    ss<<"0"<<this->stundas;
    }
    else{
    ss<<this->stundas;
    }
    ss<<":";
        if(this->minutes<10){
    ss<<"0"<<this->minutes;
    }
    else{
    ss<<this->minutes;
    }
 return ss.str();
}
    unsigned long int toStr()
    {
        //cout<<"Stundas: "<<stundas<<"; Minutes: "<<minutes<<endl;
        unsigned long int time = this->stundas * 60 + this->minutes;
        if (time > (24 * 60))
        {
            // cout<< "Laiks p?rsniedz 24h"<<endl;
        }
        return time;
    }

};
class Lidosta;

class Reiss
{
public:
    Time *izlido, *ielido;
    Lidosta *no, *uz;
    Reiss *next;

    Reiss(Lidosta *no, Lidosta *uz, Time *izlido, Time *ielido)
    {
        this->no = no;
        this->uz = uz;
        this->ielido = ielido;
        this->izlido = izlido;
        next = NULL;
    }
};

class Lidosta
{
public:
    int num;
    Reiss *reisi, *current;
    Lidosta *next;

    Lidosta(int number)
    {
        num = number;
        next = NULL;
        reisi = current = NULL;
    }

    bool checkIfReisiExit()
    {
        if (NULL == this->reisi)
        {
            return false;
        }
        return true;
    }

    void addReiss(Lidosta *no, Lidosta *uz, Time *izlido, Time *ielido)
    {

        if (!this->checkIfReisiExit())
        {
            current = reisi = new Reiss(no, uz, izlido, ielido);
        }
        else
        {
            Reiss *nq=this->reisi;
            Reiss *q=this->reisi;
            Reiss *jReiss = new Reiss(no, uz, izlido, ielido);
            for(int i=0; q!=NULL; q=q->next,i++)
            {
                if(jReiss->izlido->toStr()<q->izlido->toStr())
                {
                    if(i==0)
                    {


                        current =this->reisi=jReiss;
                        break;
                    }
                    else
                    {
                        nq->next=jReiss;
                        jReiss->next=q;
                        break;
                    }
                }
                else if(q->next==NULL){
                    q->next=jReiss;
                    break;
                }
                nq=q;
            }
        }
    }

    void removeReiss(Reiss *reiss)
    {
        this->rewindReisi();
        Reiss *nq = this->reisi;
        Reiss *q = this->reisi;
        for (int i = 0; q != NULL; q = q->next)
        {
            if (reiss->no == q->no && reiss->uz == q->uz && reiss->ielido->toStr() == q->ielido->toStr() && reiss->izlido->toStr() == q->izlido->toStr())
            {
                if (i == 0)
                {
                    this->reisi = q->next;
                    return;

                }
                nq->next = q->next;
                return;
            }
            nq = q;

        }
        delete current;
    }

    void rewindReisi()
    {
        current = this->reisi;
    }

    void printReiss(Reiss *reiss,fstream &fout)
    {
        fout << reiss->no->num << "->" << reiss->uz->num << " " << reiss->izlido->makeTime() << "-" << reiss->ielido->makeTime() << endl;
    }

    Reiss *findNext(Time *&time,fstream &fout)
    {
        this->rewindReisi();
        if (this->reisi == NULL)
        {
            return NULL;
        }
        Reiss *lq = this->reisi;
        Reiss *q = this->reisi;
        for (int i = 0; q != NULL;i++)
        {
            //cout << time->toStr() + 1 << " "<<q->no->num<<"->"<<q->uz->num<<" " << q->izlido->toStr() <<" " << q->ielido->toStr() << endl;
            if (time->toStr() + 1 <= q->izlido->toStr())
            {
                if (i == 0)
                {
                    printReiss(q,fout);
                    this->reisi = q->next;
                    return q;
                }
                printReiss(q,fout);
                lq->next = q->next;
                return q;

            }
            else
            {
                if (i != 0)
                {
                    lq = lq->next;
                }
            }
            if(q->next==NULL){
            time->stundas=0;
            time->minutes=0;
            q=this->reisi;
            i=-1;
            }
            else if(q!=NULL){

            q=q->next;
            }
            else{
            break;
            }

        }
        return NULL;
    }


};

class Lidostas
{
    Lidosta *sakums, *current;
    Lidosta *lidostas[SIZE];

    int count, no, uz, timespent;

    bool checkIfFirstExists()
    {
        if (sakums == NULL)
        {
            return false;
        }
        return true;
    }

    void initArray()
    {
        for (int i = 0; i < SIZE; i++)
        {
            lidostas[i] = NULL;
        }
    }
public:
    Time *curtime,*beginTime;

    Lidostas(int numberOfLidostas, int noLidosta, int uzLidosta
            )
    {

        initArray();
        timespent = 0;
        count = numberOfLidostas;
        no = noLidosta;
        uz = uzLidosta;
        sakums = NULL;
        beginTime=curtime = NULL;
    }

    void rewindToBegining()
    {
        current = sakums;
    }

    Lidosta *addLidosta(int lidosta)
    {
        if (!this->checkIfFirstExists())
        {
            this->lidostas[lidosta] = this->current = this->sakums = new Lidosta(lidosta);

        }
        else
        {
            if (!this->checkIfLidostaExists(lidosta))
            {

                Lidosta *jLidosta = new Lidosta(lidosta);
                this->current->next = jLidosta;
                this->current = jLidosta;
                this->lidostas[lidosta] = jLidosta;
            }

        }
        return this->lidostas[lidosta];

    }

    bool checkIfLidostaExists(int lidosta)
    {

        if (this->lidostas[lidosta] == NULL)
        {
            return false;
        }
        return true;
    }

    void loopThrough()
    {
        for (Lidosta *p = this->sakums; p != NULL; p = p->next)
        {

            cout << p->num << " ";
            for (Reiss *q = p->reisi; q != NULL; q = q->next)
            {

                cout << q->no->num << "->" << q->uz->num << " " << q->izlido->toStr() << endl;
            }
        }
        cout <<endl;
    }

    void fly(fstream &fout)
    {
        fout <<this->no<< " "<<this->beginTime->makeTime()<<endl;
        for (Lidosta *p = lidostas[this->no]; p != NULL;)
        {
            if (p->num == this->uz)
            {
                //cout << "GAL????!";
                break;
            }
            Reiss *q = p->reisi;
            if (q != NULL)
            {

                //cout << this->curtime->toStr() << endl;
                q = p->findNext(this->curtime,fout);
                if(q==NULL)
                {
                    fout.close();
fout.open("lidostas.out", ios::out);
                    fout << "Impossible";
                    break;
                }
                Lidosta *np = q->uz;
                //   cout << q->no->num << "->" << q->uz->num << " " << q->izlido->toStr() << endl;
                this->curtime = q->ielido;
                //p->removeReiss(q);
                p = np;
/*cout <<endl;
this->loopThrough();
cout <<endl;
*/
            }
            else
            {
                fout.close();
                fout.open("lidostas.out", ios::out);
                fout << "Impossible";
                break;
            }
        }

    }




};

int main()
{
    ifstream myfile;
    fstream fout;
    myfile.open("lidostas.in");
    fout.open("lidostas.out", ios::out);
    int lidSkaits, noLid, uzLid;
    char kols;
    myfile >> lidSkaits;
    myfile >> noLid;
    myfile >> uzLid;


    Lidostas *l = new Lidostas(lidSkaits, noLid, uzLid);
    int lidosta;
    Time *curtime = new Time();
    myfile >> curtime->stundas;


    myfile.get(kols);
    myfile >> curtime->minutes;
    l->beginTime=l->curtime = curtime;
    while (myfile.good() && myfile >> lidosta)
    {
        if (lidosta == 0)
        {
            break;
        }
        lidSkaits = 0;
        Lidosta *aLidosta = l->addLidosta(lidosta);
        myfile >> uzLid;
        Lidosta *uzLidosta = l->addLidosta(uzLid);
        myfile >> lidSkaits;
        //cout << lidosta << " " << uzLid << " " << lidSkaits << endl;
        for (int i = 0; i < lidSkaits; i++)
        {
            Time *obje = new Time();
            myfile >> obje->stundas;
            //cout << obje->stundas<<endl;

            myfile.get(kols);
            myfile >> obje->minutes;
            myfile.get(kols);
            Time *obj = new Time();
            myfile >> obj->stundas;
            //cout << obje->stundas<<endl;

            myfile.get(kols);
            myfile >> obj->minutes;

            aLidosta->addReiss(aLidosta, uzLidosta, obje, obj);
        }


    }

  //l->loopThrough();
    l->fly(fout);
    myfile.close();
    return 0;
}

